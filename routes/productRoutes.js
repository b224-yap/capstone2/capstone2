const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");



// Route for adding a product
router.post("/addProduct",(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization)
	if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}else {
		res.send({auth : "Not an Administrator"})
	}
});


// Retrieve all products

router.get("/allProducts", auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin)
	{
		productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth : "failed"})
	}
	
});

// Retrieve all active products
router.get("/allActiveProducts" , (req,res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
});

// Retrieving for a Specific Product

router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Update Product Information
router.put("/:productId", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth : "You are not an admin"})
	}
});

// Archive Product Admin
router.put("/archive/:productId", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.updateProductStatus(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
});


module.exports = router;