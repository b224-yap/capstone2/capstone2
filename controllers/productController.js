const Product = require("../models/Product");

// Controller Function

// Creating a New Product

module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price 
	});
	return newProduct.save().then((product,error) =>{
		if(error){
			return false
		}else {
			return true
		}
	})
}


// Retrieve all products

module.exports.getAllProducts = (data) => {
	// console.log(data.isAdmin)
	return Product.find({}).then(result => {

			return result
		})
	// if(data.isAdmin) {

		
	// }else {
	// 	return false
	// }
};

// Retrieve all Active products
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive : true}).then(result => {
		return result
	})
};

// Retrieve Specific Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
};

// Update Product Information
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct,error)=> {
		console.log(updatedProduct)
		if(error){
			return "false"
		}else {
			return "product information updated"
		}
	})
};

// Archive Product Admin
module.exports.updateProductStatus = (reqParams,reqBody) =>{
	let updatedProductStatus = {
		isActive : reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProductStatus).then((updatedProductStatus,error) => {
		console.log(updatedProductStatus)
		if(error){
			return false
		}else {
			return true
		}
	})
};