const User = require ('../models/User');
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require ("bcrypt");
const auth = require ("../auth");

// controller Functions

// User Registration

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email :reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,error)=> {
		if(error){
			return "Registration Failed"
		}else {
			return "Registration Success"
		}
	})
};


// User Log in Authentication

module.exports.loginUSer = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		if(result == null){
			return "User does not exist!"
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false
			}
		}
	})
};

// Checking Email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result =>{
		// console.log(result[0].email)
		// console.log(result.length)
		if(result.length > 0){
			return "Existing Email"
		}else {
			return "Email not Found"
		}
	})
};

// Retrieve User Detail

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result =>{
		console.log(userData.id)
		if(result == null) {
			return false
		}else {
			result.password ="*****"
			return result
		}
	})
};



